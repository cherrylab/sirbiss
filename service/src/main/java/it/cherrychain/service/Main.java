package it.cherrychain.service;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Verticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.concurrent.atomic.AtomicReference;

public enum Main {
  ;
  private static final String s3host = "https://sirbiss-config.s3.eu-south-1.amazonaws.com";
  private static final String s3file = "/sirbiss-config-file";

  public static void main(String[] args) {
    final var vertx = Vertx.vertx();

    final var deploymentId = new AtomicReference<>("0");
    final var retriever = retriever(vertx);
    retriever.listen(change -> {
      JsonObject config = change.getNewConfiguration();
      System.out.printf("I read a config: %n%s%n", config.encodePrettily());
      final var vertxWorker = config.getBoolean("VERTX_WORKER", false);
      final var name = config.getJsonObject("greeting", new JsonObject().put("name", "Pantagruele")).getString("name");

      switch (deploymentId.get()) {
        case "0" -> deployVerticle(vertx, vertxWorker, name)
          .onSuccess(deploymentId::set);
        default -> vertx.undeploy(deploymentId.get())
          .compose(__ -> deployVerticle(vertx, vertxWorker, name))
          .onSuccess(deploymentId::set);
      }
    });
  }

  private static Future<String> deployVerticle(Vertx vertx, Boolean vertxWorker, String name) {
    return vertx.deployVerticle(Greeting.verticle(name), new DeploymentOptions().setWorker(vertxWorker));
  }

  private static ConfigRetriever retriever(Vertx vertx) {
    return ConfigRetriever.create(vertx, new ConfigRetrieverOptions()
      .setScanPeriod(1000)
      .addStore(s3Store())
      .addStore(envStore())
    );
  }

  private static ConfigStoreOptions s3Store() {
    return new ConfigStoreOptions()
      .setType("http")
      .setFormat("hocon")
      .setConfig(new JsonObject()
        .put("host", "sirbiss-config.s3.eu-south-1.amazonaws.com")
        .put("port", 443)
        .put("ssl", true)
        .put("path", "%s%s".formatted(s3host, s3file))
      );
  }

  private static ConfigStoreOptions envStore() {
    return new ConfigStoreOptions()
      .setType("env")
      .setConfig(new JsonObject()
        .put("keys", new JsonArray().add("VERTX_WORKER").add("DESKTOP_SESSION"))
      );
  }
}

final class Greeting extends AbstractVerticle implements Verticle {
  private final String name;

  static Verticle verticle(String name) {
    return new Greeting(name);
  }

  private Greeting(String name) {
    this.name = name;
  }

  @Override
  public void start(Promise<Void> start) {
    System.out.printf("Hello %s!%n", name);
    start.complete();
  }

  @Override
  public void stop(Promise<Void> stop) {
    System.out.printf("Goodbye %s!%n", name);
    stop.complete();
  }
}
