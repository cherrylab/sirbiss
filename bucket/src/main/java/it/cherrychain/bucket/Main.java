package it.cherrychain.bucket;

import com.pulumi.Pulumi;
import com.pulumi.asset.FileAsset;
import com.pulumi.aws.s3.Bucket;
import com.pulumi.aws.s3.BucketArgs;
import com.pulumi.aws.s3.BucketObject;
import com.pulumi.aws.s3.BucketObjectArgs;
import com.pulumi.aws.s3.BucketPolicy;
import com.pulumi.aws.s3.BucketPolicyArgs;
import com.pulumi.resources.CustomResourceOptions;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

public enum Main {;
  private static final String s3url = "https://%s.s3.eu-south-1.amazonaws.com/%s";

  public static void main(String[] args) {
    Pulumi.run(aws -> {
      final var bucket = new Bucket("sirbiss-bucket", BucketArgs.builder()
        .bucket("sirbiss-config")
        .build());

      final var fileUrl = Optional.ofNullable(Main.class.getClassLoader().getResource("application.conf"))
        .map(URL::getFile);
      final var fileAsset = fileUrl.map(FileAsset::new).orElseThrow();

      final var name = "sirbiss-config-file";
      final var applicationConf = new BucketObject(name,
        BucketObjectArgs.builder()
          .bucket(bucket.bucket())
          .acl("public-read")
          //.contentType(fileUrl.map(Main::contentType).orElseThrow())
          .source(fileAsset)
          .build(),
        CustomResourceOptions.builder()
          .parent(bucket)
          .build()
      );

/*      var policy = new BucketPolicy("sirbiss-policy",
        BucketPolicyArgs.builder()
          .bucket(bucket.bucket())
          .policy(bucket.bucket().applyValue("""
            {
              "Version": "2012-10-17",
              "Statement": [{
                "Effect": "Allow",
                "Principal": "*",
                "Action": ["s3:GetObject"],
                "Resource": ["arn:aws:s3:::%s/*"]
              }]
            }
            """::formatted)
          )
          .build()
      );*/

      aws.export("application-conf", bucket.bucket().applyValue(it -> s3url.formatted(it, name)));
    });
  }

  private static String contentType(String it) {
    try {
      return Files.probeContentType(Path.of(it));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
