# Alpenglow

Experiments for full continuous integration and deployment

## Settings:

Configure the following envs for getting to work in `AWS`:

* `AWS_REGION` for deployment zone area
* `AWS_SECRET_ACCESS_KEY` for secret access key
* `AWS_ACCESS_KEY_ID` for access key
